

# Architecture

6 services sont déployé :
- Authentifcation, développé en Python (django). 
- Recherche, ????
- Api, développé en Java
- Front, développé en Angular avec le framework Clarity
- Postgres, une base de données pour l'authentification
- MySQL, une base de données pour l'Api

Le tout est déployé grâce à Ansible dans des conteneurs Docker orchestré par Swarm.
Cela nous donne donc un moyen de déployer facilement et à la demande notre infrastructure sur un nombre de serveurs illimité. Le tout est scalable à l'infini.

Tout nos services communiquent grâce à des apis RESTFull et que l'on utilise un token JWT pour l'authentification entre les différents services.

# Deploiement

Pour la partie déploiement se référer aux README du sous module ansible.

# Test

L'api est testé unitairement, les tests sont lancés automatiquement à chaque push. Grâce à notre cycle CI/CD.

# CI/CD

Nous utilisons gitlab pour le gestionnaire de version ainsi que l'intégration continue et le déploiement continue.
Plusieurs étapes sont passé : build, test, deploy (varie en fonction du service)
Le tout est déployé automatiquement.